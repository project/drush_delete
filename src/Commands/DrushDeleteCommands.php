<?php

namespace Drupal\drush_delete\Commands;

use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile for Drush Delete All module.
 */
class DrushDeleteCommands extends DrushCommands {

  /**
   * Delete all contents.
   *
   * @param $type
   *   The entity type name
   * @validate-module-enabled drush_delete
   *
   * @command delete:all
   * @aliases delete-all
   */
  public function all($type) {
    $this->output()->writeln(\Drupal::service('drush_delete.entity')->deleteAllEntityType($type));
  }

  /**
   * Delete selected Taxonomy Vocabulary terms.
   *
   * @param $type
   *   The taxonomy vocabulary name
   * @validate-module-enabled drush_delete
   *
   * @command delete:all-taxonomy-vocabulary-term
   * @aliases delete-all-taxonomy-vocabulary-term
   */
  public function allTaxonomyVocabularyTerm($type) {
    $this->output()->writeln(\Drupal::service('drush_delete.entity')->deleteAllTerms($type));
  }

}
