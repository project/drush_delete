<?php

namespace Drupal\drush_delete;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Database\Connection;

/**
 * Delete content service class.
 */
class DeleteContent {

  /**
   * The message to be shown to the user.
   *
   * @var string
   */
  protected $saysomething;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * DeleteContent constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager object.
   * @param \Drupal\Core\Database\Connection $connection
   *   The Connection object containing the key-value tables.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $connection) {
    $this->saysomething = 'Hello World!';
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
  }

  /**
   * Displays the Hello message.
   */
  public function sayHello($name = '') {
    if (empty($name)) {
      return $this->saysomething;
    }
    else {
      return "Hello " . $name . "!";
    }
  }

  /**
   * Function that provides the deleteing content functionality.
   */
  public function deleteAllEntityType($type) {
    $nids = $this->getEntityIds($type);
    if (!empty($nids)) {
      $no_delete = 0;
      foreach ($nids as $nid) {
        $this->entityTypeManager->getStorage('node')->load($nid)->delete();
        $no_delete++;

      }

      return $no_delete . " no of contents are deleted";
    }
    else {
      return "Content not found !";
    }
  }

  /**
   * Provides the Entity Ids of the given type.
   *
   * @param string $type
   *   Entity type.
   *
   * @return array|Null
   *   The array of all the nids.
   */
  public function getEntityIds($type) {
    $nids = [];
    $result = $this->connection->select('node', 'n')
      ->fields('n', ['nid'])
      ->condition('type', $type, '=')
      ->execute()->fetchall();
    if (empty($result)) {
      return NULL;
    }
    else {
      foreach ($result as $item) {
        $nids[] = $item->nid;
      }
    }

    return $nids;
  }

  /**
   * To delete the entity content.
   *
   * @param string $type
   *   Entity type.
   *
   * @return string
   *   The message to be displayed when entity contents are deleted.
   */
  public function deleteAllEntity($type) {
    $query = $this->entityTypeManager->getStorage($type)->getQuery();
    $nids = $query->execute();
    $no_delete = 0;
    if (!empty($nids)) {
      foreach ($nids as $nid) {
        $this->entityTypeManager->getStorage($type)->load($nid)->delete();
        $no_delete++;
      }
      return $no_delete . " entities deleted.";
    }
    else {
      return "Nothing to delete.";
    }
  }

  /**
   * To delete the taxonomy vocabulary terms.
   *
   * @param string $type
   *   Entity type.
   *
   * @return string
   *   The message to be displayed when terms are deleted.
   */
  public function deleteAllTerms($type) {
    $tids = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery()
      ->condition('vid', $type)
      ->execute();
    if (!empty($tids)) {
      $count = count($tids);
      $controller = $this->entityTypeManager->getStorage('taxonomy_term');
      $entities = $controller->loadMultiple($tids);
      $controller->delete($entities);
      return $count . " terms deleted.";
    }
    else {
      return "Nothing to delete.";
    }

  }

}
