CONTENTS OF THIS FILE
---------------------

 * INTRODUCTION
 * REQUIREMENTS
 * INSTALLATION
 * CONFIGURATION
 * MAINTAINERS


INTRODUCTION
------------

* This module provides the drush command to delete all the
  dummy contents through drush Command.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.
  Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

This module adds three drush commands to use. After enabling this module,
refresh drush by using the command `drush cc drush`.

`drush delete-all entity-type-machine-name`
Delete all contents of a entity type argument ('entity-type-machine-name').

`drush delete-all-entity entity-type-machine-name'
Mirror of `delete-all`.

`drush delete-all-taxonomy-vocabulary-term vocab-machine-name`
Delete selected taxonomy Vocabulary terms from argument (`vocab-machine-name`).


MAINTAINERS
-----------

Current maintainers:
 * Rakesh James (rakesh.gectcr) - https://www.drupal.org/u/rakeshgectcr
